import trio
import trio_websocket

async def connect() -> None:
    print('starting')
    async with trio_websocket.open_websocket_url('ws://127.0.0.1:8000/ws') as ws:
        print('connected?')
        print(await ws.get_message())

trio.run(connect)

