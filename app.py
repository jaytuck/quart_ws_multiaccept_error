import asyncio
from quart import Quart, websocket

app = Quart(__name__)

class SlowMiddleware:
    '''this middleware simulates a slow websocket'''
    def __init__(self, app):
        self.app = app 

    async def __call__(self, scope, receive, send):
        async def slowsend(event):
            print(f'snoozing')
            await asyncio.sleep(1)
            await send(event)
        return await self.app(scope, receive, slowsend)

app.asgi_app = SlowMiddleware(app.asgi_app)

@app.websocket("/ws")
async def ws():
    print('connection started')
    #await websocket.accept()
    t1 = asyncio.create_task(websocket.send("hi"))
    t2 = asyncio.create_task(websocket.send("hi2"))
    #await websocket.send("")
    await asyncio.gather(t1, t2) 

